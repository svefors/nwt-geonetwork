package org.fao.geonet.kernel.harvest.harvester;

import org.geotools.gml3.v3_2.gco.GCO;
import org.geotools.gml3.v3_2.gmd.GMD;
import org.jdom.Element;

public class HierarchyLevelFactory {

    /**
     * util class has private constructor.
     */
    private HierarchyLevelFactory() {
    }

    public static Element createHierarchyLevelElement(String mdScopeCodeValue) {
        Element hierarchyLevel =
                new Element("hierarchyLevel", GMD.NAMESPACE)
                        .setContent(new Element("MD_ScopeCode", GMD.NAMESPACE)
                                .setAttribute(
                                        "codeList",
                                        "http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode"
                                )
                                .setAttribute(
                                        "codeSpace",
                                        "ISOTC211/19115")
                                .setText(mdScopeCodeValue)
                                .setAttribute("codeListValue", mdScopeCodeValue));
        return hierarchyLevel;
    }

    public static Element createHierarchyLevelNameElement(String hierarchyLevelNameValue) {
        return new Element("hierarchyLevelName", GMD.NAMESPACE)
                .setContent(
                        new Element("CharacterString", GCO.NAMESPACE)
                                .setText(hierarchyLevelNameValue)
                );
    }
}
