package org.fao.geonet.kernel.harvest.harvester.localfilesystem;

import jeeves.server.context.ServiceContext;
import org.apache.commons.codec.binary.Base64;
import org.fao.geonet.Logger;
import org.fao.geonet.kernel.DataManager;
import org.fao.geonet.kernel.harvest.harvester.HarvestResult;
import org.fao.geonet.utils.BinaryFile;
import org.fao.geonet.utils.Xml;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class ThumbnailContentUtil {
    /**
     * Private Constructor for utility class with only static methods
     */
    private ThumbnailContentUtil() {
    }

    /**
     * extract the thumbnail content from the XML tree.
     *
     * @param metadataElement
     * @return the binary data of the thumbnail if present or empty string
     * @throws JDOMException
     */
    static Optional<String> thumbnailContent(Element metadataElement) throws JDOMException {
        // Extract picture if available
        List<Namespace> esriMdNamespaces = new ArrayList<>();
        esriMdNamespaces.add(metadataElement.getNamespace());
        esriMdNamespaces.addAll(metadataElement.getAdditionalNamespaces());

        // select all nodes that match the XPath
        Element thumbnailEl = Xml.selectElement(metadataElement, "Binary/Thumbnail/Data", esriMdNamespaces);
        return Optional.ofNullable(thumbnailEl).map(Element::getText);
    }

    static Path createThumbnailPath(ServiceContext context, String uuid) {
        String filename = uuid + ".png";
        Path dir = context.getUploadDir();
        return dir.resolve(filename);
    }

    static OutputStream createThumbnailOutputStream(ServiceContext context, String uuid) throws IOException {
        String filename = uuid + ".png";
        Path dir = context.getUploadDir();
        return Files.newOutputStream(dir.resolve(filename));
    }

    static Element createParElementForThumbnail(String metadataId, String filename) {
        Element par = new Element("request");
        par.addContent(new Element("id").setText(metadataId));
        par.addContent(new Element("version").setText("10"));
        par.addContent(new Element("type").setText("large"));

        Element fname = new Element("fname").setText(filename);
        fname.setAttribute("content-type", "image/png");
        fname.setAttribute("type", "file");
        fname.setAttribute("size", "");

        par.addContent(fname);
        par.addContent(new Element("add").setText("Add"));
        par.addContent(new Element("createSmall").setText("on"));
        par.addContent(new Element("smallScalingFactor").setText("180"));
        par.addContent(new Element("smallScalingDir").setText("width"));
        return par;
    }

    /**
     * This method will create a PNG image from the thumbnail content and add a link in the metadata to
     * the thumbnail. In other words, it has lots of intended side-effects. It was Copied from Geonetwork repository ArcSDEHarvester.
     * git tag: 3.2.2, commit hash: dde7f790.
     *
     * @param log
     * @param context
     * @param dataMan
     * @param result
     * @param metadataId
     * @param uuid
     */
    static void uploadThumbnail(Logger log, ServiceContext context, DataManager dataMan, HarvestResult result, String thumbnailContent, String metadataId, String uuid) {
        log.info("  - Creating thumbnail for metadata uuid: " + uuid);

        try {
//            Path thumbnailPath = createThumbnailPath(context, uuid);
            String filename = uuid + ".png";
            byte[] thumbnailImg = Base64.decodeBase64(thumbnailContent);

            try (OutputStream fo = createThumbnailOutputStream(context, uuid);
                 InputStream in = new ByteArrayInputStream(thumbnailImg);) {
                BinaryFile.copy(in, fo);
            }

            if (log.isDebugEnabled()) log.debug("  - File: " + filename);

            Element par = createParElementForThumbnail(metadataId, filename);

            // Call the services
            org.fao.geonet.services.thumbnail.Set s = new org.fao.geonet.services.thumbnail.Set();
            s.execOnHarvest(par, context, dataMan);

            dataMan.flush();

            result.thumbnails++;


        } catch (Exception e) {
            log.warning("  - Failed to set thumbnail for metadata: " + e.getMessage());
            e.printStackTrace();
            result.thumbnailsFailed++;
        }
    }
}
